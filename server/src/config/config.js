module.exports = {
  port: 8081,
  dbURL: 'mongodb://localhost:27017/front',
  dbOptions: { useMongoClient: true }
}
